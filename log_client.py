
import socket
import threading
import time
import sys

import pygtk
pygtk.require("2.0")
import gtk
import pango

class Client(threading.Thread):
	def __init__(self, client, ipaddr, app):        		
		self.stopthread = threading.Event()
		self.client = client
		self.ipaddr = ipaddr		
		self.app = app
		app.open_tab(ipaddr)
		self.app.set_tab_title_suffix(self.ipaddr, "[C]")
		threading.Thread.__init__(self)
	
	def run(self):
		while not self.stopthread.isSet():
			try:
				data = self.client.recv(1024)
				if not data:
					break
				self.app.write_to_log(self.ipaddr, data)
			except:
				pass
		self.app.set_tab_title_suffix(self.ipaddr, "[D]")
		self.client.close()
		print("Client " + self.ipaddr + " thread done.")
		
	def stop(self):
		self.stopthread.set()

class Server(threading.Thread):

	def __init__(self, app):        
		self.stopthread = threading.Event()
		self.app = app 
		threading.Thread.__init__(self)
		
	def run(self):		
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)		
		self.server.bind(("",4444))
		self.server.listen(5)		
		self.server.settimeout(1)
		print("Sever ready. Waiting for connections.")
		while not self.stopthread.isSet():			
			try:
				(client, address) = self.server.accept()				
				self.server.settimeout(1)
				print("Got connection from " + address[0])
				client_thread = Client(client, address[0], self.app)
				client_thread.start()
			except socket.timeout:
				pass
		print("Server thread stopped.")
	
	def stop(self):
		self.server.close()
		self.stopthread.set()

class LogPage():
	
	def __init__(self, app, title):
		self.app = app
		self.title = title
		self.title_suffix = ""

		self.app.tabs[self.title] = self

		self.scrollwindow = gtk.ScrolledWindow()
		self.scrollwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.textbuffer = gtk.TextBuffer(table=None)
		self.textview = gtk.TextView(buffer=self.textbuffer)
		self.textview.set_editable(False)
		self.textview.set_wrap_mode(gtk.WRAP_NONE)
		self.textview.set_size_request(500,400)
		self.font = pango.FontDescription("monospace 11")
		self.textview.modify_font(self.font)
		self.textview.show()
		self.scrollwindow.add(self.textview)
		self.scrollwindow.set_size_request(500,400)
		self.scrollwindow.show()

		# prepare tab bar button
		close_btn = gtk.Button()
		close_btn.set_relief(gtk.RELIEF_NONE)
		close_btn.set_focus_on_click(False)
		close_image = gtk.image_new_from_stock(gtk.STOCK_CLOSE, gtk.ICON_SIZE_MENU)
		close_btn.add(close_image)
		btn_style = gtk.RcStyle()
		btn_style.xthickness = 0
		btn_style.ythickness = 0
		close_btn.modify_style(btn_style)
		close_btn.connect("clicked", self.on_close_btn_clicked, self.scrollwindow)

		self.page_label= gtk.Label(title)

		self.page_box = gtk.HBox(False, 0)
		self.page_box.pack_start(self.page_label)
		self.page_box.pack_start(close_btn, False, False)
		self.page_box.show_all()
		
		self.page_no = self.app.notebook.append_page(self.scrollwindow, self.page_box)

	def close(self):
		self.app.notebook.remove_page(self.page_no)
		self.app.notebook.queue_draw_area(0,0,-1,-1)
		del self.app.tabs[self.title]

	def reset(self):
		self.textbuffer.set_text("")

	def set_title(self, new_title):
		if self.title != new_title:
			self.page_label.set_text(new_title + self.title_suffix)
			del self.app.tabs[self.title]
			self.title = new_title
			self.app.tabs[self.title] = self

	def set_title_suffix(self, title_suffix):
		if self.title_suffix != title_suffix:
			self.title_suffix = title_suffix
			self.page_label.set_text(self.title + self.title_suffix)

	def on_close_btn_clicked(self, sender, scrollwindow):
		self.close()

	def write_text(self, text):
		it = self.textbuffer.get_end_iter()
		text = text.replace('\x00', '').decode('utf-8', 'replace').encode('utf-8')
		self.textbuffer.insert(it, text)
		self.textview.scroll_to_iter(it, 0.1)
	
		
class Application():

	def __init__(self):
		gtk.threads_init()

		self.tabs = { }

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.connect("delete_event", self.closed)
		self.window.set_border_width(5)
		self.window.set_title("Super Logger 4444")

		self.table = gtk.Table(3,6,False)
		self.window.add(self.table)

		self.notebook = gtk.Notebook()
		self.notebook.set_tab_pos(gtk.POS_TOP)
		self.table.attach(self.notebook, 0,6,0,1)
		self.notebook.show()

		self.table.show()
		self.window.show()

		self.server = Server(self)
		self.server.start()

		gtk.main()
		
	def closed(self, widget, event=None):
		self.server.stop()
		gtk.main_quit()
		return False

	def open_tab(self, title):
		gtk.threads_enter()
		if title in self.tabs:
			new_tab = self.tabs[title]
			new_tab.set_title_suffix("[C]")
			new_tab.reset()
		else:
			new_tab = LogPage(self, title)
		gtk.threads_leave()
		
	def close_tab(self, title):
		gtk.threads_enter()
		self.tabs[title].close()
		gtk.threads_leave()

	def write_to_log(self, title, text):
		if title in self.tabs:
			gtk.threads_enter()
			self.tabs[title].write_text(str(time.time()) +"> " +text)
			gtk.threads_leave()

	def set_tab_title_suffix(self, tabid, suffix):
		if tabid in self.tabs:
			gtk.threads_enter()
			self.tabs[tabid].set_title_suffix(suffix)
			gtk.threads_leave()

def main(argv):	
	Application()	
	print("Application done.")
	return 0    

if __name__ == "__main__":
	sys.exit(main(sys.argv))



